import abc
import pandas as pd
import pickle
from concurrent.futures import ThreadPoolExecutor
import datetime
import subprocess as subp
import CheckRepLib as crl

'''
Psudocode - high level....
command design pattern to allow obfuscation of complexity and commands and to implement threading
of processess to increase speed of the program.

get variables for automatic horcm group collection
run raidqry output groups and horcm numbers to a pickle for future use
cycle through pickle running horcm check 'pairvolchk' outputing status, group, and horcm number to
log or file 
email log file to service now.
email group of un-paired volumes for immediate notification.

Testing performed - horcm instance checking not yet implemented

'''

def main():

    # setting variables
    comLib = crl.Commonlib()
    comLib.set_mail_address_to('Graham.Dunkley@iag.com.au')
    comLib.set_mail_smtp_server('10.139.55.55')
    comLib.set_logpath('')

    # url = 'https://10.139.188.113:8097/api/now/table/em_event'
    # user = 'eventapprest'
    # pwd = '3v3NT4ppr3ST!'

    receiver_horcm = ReceiverHorcm(comLib)
    receiver_raidqry = ReceiverRaidqry(comLib)
    receiver_pairvolchk = ReceiverPairvolchk(comLib)

    # creating command objects
    command_horcm = CommandHorcm(receiver_horcm)
    command_raidqry = CommandRaidqry(receiver_raidqry)
    command_pairvolchk = CommandPairvolchk(receiver_pairvolchk)

    # initializing the invoker object
    invoker_horcm = Invoker(command_horcm)
    print('step - invoker_horcm')                                           # testing print
    # creating/invoking the raidqry command to get each replication group
    invoker_horcm.set_command(command_raidqry)
    invoker_horcm.invoke()
    print('step - invoker_horcm-command_raidqry')                           # testing print
    # creating/invoking the pairvolckh methods to build commands to run the check.
    invoker_horcm.set_command(command_pairvolchk)
    invoker_horcm.invoke()
    print('step - invoker_horcm-pairvolchk')                               # testing print
    # invoking/running the replication checking on each group, outputing results based on result conditions.
    invoker_horcm.set_command(command_horcm)
    invoker_horcm.invoke()
    print('step - invoker_horcm-command_horcm')                           # testing print

class CommandInterface(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def execute(self):
        pass


class ReceiverHorcm:

    def __init__(self, commonlib_variables):
        self.clib = commonlib_variables

    def check_horcm(self):

        pickle_in_horcmdetail = open('horcmdetail.pickle', 'rb')
        df_horcmdetail = pickle.load(pickle_in_horcmdetail)
        path = self.clib.get_logpath()
        file_fullpath = path + 'replication.log'
        self.clib.file_opentowrite(file_fullpath)

        threadcount = 1
        maxworkers_count = 50
        with ThreadPoolExecutor(max_workers=maxworkers_count) as executor:
            for row in df_horcmdetail.itertuples():
                if maxworkers_count < threadcount:
                    threadcount = 1
                print('Running Thread: ' + str(threadcount))
                args = {
                    'cmd': row[7],
                    'server': row[6],
                    'group': row[1],
                    'horcmid': str(row[2]),
                    'copytype': row[4],
                    'site': row[3],
                    'sourceDest': row[5]
                }
                executor.submit(self.run_pairvolchk(args))

                print('Completed Thread: ' + str(threadcount) + ' with cmd ' + row[7] + '\n')
                threadcount += 1

        self.clib.file_close()

    def run_pairvolchk(self, params):
        print('checking volstatus...')

        cmd = params.get('cmd')
        server = params.get('server')
        group = params.get('group')
        horcmid = params.get('horcmid')
        copytype = params.get('copytype')
        site = params.get('site')
        sourceDest = params.get('sourceDest')

        #print('args: ', cmd, server, group, horcmid, copytype, site, sourceDest)

        curdate = datetime.datetime.now()
        curdate = curdate.strftime('%Y:%m:%d_%H:%M:%S ')
        cmd = cmd.replace('\n', '')
        self.clib.set_command(cmd)

        try:
            df_volchk = crl.Commonlib.run_shell_cmd(self.clib, 0)
            #print('dfvolchk values: ', df_volchk)
        except subp.CalledProcessError as e:
            print(e.output)

        df_volchk[1] = df_volchk[1].str[:-1]
        msg = df_volchk.iloc[0][0] + df_volchk.iloc[0][1]
        msg1 = df_volchk.iloc[0][0]

        self.clib.set_mail_subject(msg)
        df_volchk.rename(columns={0: 'desc'}, inplace=True)
        df_voltmp = df_volchk[1].str.split(' ', n=12, expand=True)
        df_volchk.drop(df_volchk.columns[[1]], axis=1, inplace=True)

        # build mail/event description details
        message_body = str('\n' + msg + '\n' +
                           '\ndateTime=' + curdate +
                           '\nserver = ' + server +
                           '\ngroup = ' + group +
                           '\nhorcmId = ' + horcmid +
                           '\ncopyType = ' + copytype +
                           '\nsite = ' + site +
                           '\nsource_destination = ' + sourceDest +
                           '\n' + msg1)

        col_len = len(df_voltmp.columns)
        df_volchk['status'] = df_voltmp[2]
        message_body = message_body + '\nstatus = ' + df_volchk.iloc[0]['status']

        if col_len >= 5:
            df_volchk['fence'] = df_voltmp[5]
            message_body = message_body + '\nstatus = ' + df_volchk.iloc[0]['fence']
        if col_len >= 8:
            df_volchk['CTGID'] = df_voltmp[8]
            message_body = message_body + '\nstatus = ' + df_volchk.iloc[0]['CTGID']
        if col_len >= 11:
            df_volchk['MINAP'] = df_voltmp[11]
            message_body = message_body + '\nstatus = ' + df_volchk.iloc[0]['MINAP']

        self.clib.set_mail_message(message_body)

        # Build log message
        log_message = str(curdate + df_volchk.iloc[0]['status'] +
                          ' {grp=' + group + ',srv=' + server +
                          ',horcm=' + horcmid + '}[' + msg + ']\n')

        # Build http rest message
        message_key = group + '_' + horcmid


        if copytype == 'SI':
            event_class = 'BlockRep_SI'
        else:
            event_class = 'BlockRep_GAD'

        # Compare replication status and send email alert and or log replication status.
        val = df_volchk.iloc[0]['status']
        if val == 'PAIR':
            self.clib.file_writetofile(log_message)
            print(log_message)

            severity = 5

            args_http = {'event_class': event_class, 'heading': msg, 'description': message_body,
                         'message_key': message_key, 'severity': severity}
            # run http alert
            self.clib.http_event_msg(args_http)

        else:

            if copytype == 'SI':
                severity = 4
            else:
                severity = 2

            args_http = {'event_class': event_class, 'heading': msg, 'description': message_body,
                         'message_key': message_key, 'severity': severity}
            # run htp alert
            self.clib.http_event_msg(args_http)


            # Email Alerting through sendEmail app.
            # print('SENDING Alert MAIL NOTIFICATION...')
            # cmd_mail = 'sendEmail - f %HitachiVantara_Onsite@iag.com.au% -t %' +\
            #            self.clib.get_mail_address_to() +\
            #            '% -u "' +\
            #            self.clib.get_mail_subject() +\
            #            '" - m "' +\
            #            self.clib.get_mail_message() +\
            #            '" - s %' +\
            #            self.clib.get_mail_smtp_server() + '%'
            # # self.clib.run_shell(cmd_mail)
            # print(cmd_mail)


class ReceiverRaidqry:

    def __init__(self, commonlib_variables):
        self.clib = commonlib_variables

    def check_raidqry(self):
        df_hinst = pd.DataFrame()
        df_hinst['horcId'] = None
        df_hinst_adj = pd.DataFrame()
        pickle_horcm_detail = open('horcmdetail.pickle', 'wb')

        print("raidqry commands...")
        pth1 = crl.Commonlib.os_check()

        with open('horcmNumbers.txt', 'r') as file:
            # with ThreadPoolExecutor(max_workers=8) as executor:       # need to implement multithreadding here...
            for inst in file:
                inst = inst.replace('\n', '')                       # removal of new line char from windows.

                self.clib.set_horcmID(inst)
                horc_instance = str(self.clib.get_horcmID())
                cmd = pth1 + 'raidqry -g ' + '-IH' + horc_instance
                self.clib.set_command(cmd)

                try:
                    df_hinst = df_hinst.append(crl.Commonlib.run_shell_cmd(self.clib, 1), ignore_index=True)
                    # print('df_hinst value: ', df_hinst)
                    print('horcid: ', horc_instance, '\ndf_hinst value:\n', df_hinst)
                except subp.CalledProcessError as e:
                    print(e.output)

                if not df_hinst.empty:
                    #df_hinst['horcId'].fillna(horc_instance)
                    df_hinst.fillna(value=horc_instance, inplace=True)
                    # df_hinst['horcId'] = horc_instance
                    df_hinst = df_hinst.replace('\n', '', regex=True)
                    print('horcid: ', horc_instance, '\ndf_hinst value:\n', df_hinst)

                df_hinst_adj.append(df_hinst, ignore_index=True)
                print(df_hinst_adj)
        print(df_hinst_adj)
        if not df_hinst.empty:

            df_hinst = df_hinst[[1, 'horcId']]
            df_hinst.rename(columns={1: 'group'}, inplace=True)

            # use a separate df to extract out additional detail from group
            df_tmp = df_hinst['group'].str.split('_', n=1, expand=True)
            df_tmp.columns = [0, 'server']

            # Data munging to get easy values for the volume check commands.
            df_hinst['site'] = df_tmp[0].str.slice(0, 1, 1)
            df_hinst['copy'] = df_tmp[0].str.slice(1, 3, 1)
            df_hinst['src-dest'] = df_tmp[0].str.slice(3, 8, 1)
            df_hinst['server'] = df_tmp['server']

            # pickle df with changes for details.
            pickle.dump(df_hinst, pickle_horcm_detail)
            pickle_horcm_detail.close()

            del df_hinst, df_tmp


class ReceiverPairvolchk:

    def __init__(self, commonlib_variables):
        self.clib = commonlib_variables

    def build_pairvolchk(self):
        print("Building pairvolchk cmds...")

        pickle_in_horcm_detail = open('horcmdetail.pickle', 'r+b')
        pickle_out_horcm_detail = open('horcmdetail.pickle', 'r+b')

        df_horcmdetail = pickle.load(pickle_in_horcm_detail)
        pickle_in_horcm_detail.close()

        pth1 = crl.Commonlib.os_check()
        df_horcmdetail['cmd'] = None

        for row in df_horcmdetail.itertuples():
            horcgroup = row[1]
            horcId = row[2]
            copy = row[4]

            if copy == 'SI':
                horcAccess = ' -ISI'
            else:
                horcAccess = ' -IH'

            cmd = pth1 + 'pairvolchk -g ' + horcgroup + horcAccess + horcId
            # using row.Index as the loop is using the itertuples function.
            df_horcmdetail.at[row.Index, 'cmd'] = cmd
        print(df_horcmdetail)                   # testing print
        pickle.dump(df_horcmdetail, pickle_out_horcm_detail)
        pickle_out_horcm_detail.close()


class CommandHorcm(CommandInterface):

    def __init__(self, horcm: ReceiverHorcm):
        self._horcm = horcm

    def execute(self):
        self._horcm.check_horcm()


class CommandRaidqry(CommandInterface):

    def __init__(self, raidqry: ReceiverRaidqry):
        self._raidqry = raidqry

    def execute(self):
        self._raidqry.check_raidqry()


class CommandPairvolchk(CommandInterface):

    def __init__(self, pairvolchk: ReceiverPairvolchk):
        self._pairvolchk = pairvolchk

    def execute(self):
        self._pairvolchk.build_pairvolchk()


class Invoker:

    def __init__(self, command: CommandInterface):
        self._command = command
        self._command_list = []

    def set_command(self, command: CommandInterface):
        self._command = command

    def get_command(self):
        print(self._command.__class__.__name__)

    def add_command_to_list(self, command: CommandInterface):
        self._command_list.append(command)

    def execute_commands(self):

        for cmd in self._command_list:
            cmd.execute()

        self._command_list.clear()

    def invoke(self):
        self._command.execute()








if __name__ == '__main__':
    main()
