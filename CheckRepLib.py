from sys import platform
import subprocess
import subprocess as subp
from io import StringIO
import pandas as pd
import requests


class Commonlib:

    def __init__(self):
        self.horcmID = None
        self.horcmGrp = None
        self.command = None
        self.server = None
        self.copy = None
        self.site = None
        self.srcdest = None
        self.mail_address_to = None
        self.mail_subject = None
        self.mail_message = None
        self.mail_smtp_server = None
        self.file_write = None
        self.logpath = None


    def file_opentowrite(self, filename):
        file = filename
        self.file_write = open(file, 'a+')

    def file_writetofile(self, msg):
        file_write = self.file_write
        file_write.write(msg)
        # file_write.append(msg)

    def file_close(self):
        self.file_write.close()

    def http_event_msg(self, params):

        #req_source = params.get('source')
        req_event_class = params.get('event_class')
        req_heading = params.get('heading')
        req_description = params.get('description')
        #req_metric_name = params.get('metric_name')
        req_message_key = params.get('message_key')
        req_severity = params.get('severity')

        url = 'https://10.139.188.113:8097/api/now/table/em_event'
        user = 'eventapprest'
        pwd = '3v3NT4ppr3ST!'
        headers = {"Content-Type": "application/json", "Accept": "application/json"}
        data_post = {'type': 'Storage',
                     'source': 'storage_replication',
                     'event_class': req_event_class,
                     'short_description': req_heading,
                     'description': req_description,
                     'message_key': req_message_key,
                     'metric_name': 'horcm',
                     'severity': req_severity}

        response = requests.post(url, auth=(user, pwd), headers=headers, data=data_post)

        # response = requests.post(url,
        #                          auth=(user, pwd),
        #                          headers=headers,
        #                          data="{\"type\":\"Storage\","
        #                               "\"source\":\"REST\","
        #                               "\"event_class\":\"Source Instance\","
        #                               "\"short_description\":\"headding\","
        #                               "\"description\":\"A good description\","
        #                               "\"message_key\":\"a unique key\","
        #                               "\"metric_name\":\"metric name\","
        #                               "\"severity\":\"1\"}")

        if response.status_code != 200:
            print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:', response.json())
            exit()

        data = response.json()
        print(data)

    def os_check():

        if platform == "linux" or platform == "linux2":
            # print("linux or linux2")
            ospath = '/HORCM/etc/'
        elif platform == "darwin":
            # print("OSX")
            ospath = '/HORCM/etc/'
        elif platform == "win32":
            # print("Wind")
            ospath = '/HORCM/etc/'

        return ospath

    def run_shell_cmd(self, opt):
        # opt setting should be either 0 or 1
        df = pd.DataFrame()
        cmdtst = self.command
        #print(cmdtst)
        try:
            process = subp.Popen(cmdtst, stdin=None, stdout=subp.PIPE, stderr=subp.DEVNULL)
            #print('process: ', process)                                      # testing print
            result, err = process.communicate(timeout=5)
            #print('result: ', result)                                      # testing print
            #print('err: ', err)                                      # testing print
            out = StringIO(result.decode('utf-8'))
            #print('out: ' + str(out))                                      # testing print
            code = process.returncode
            #print('returncode: ', code)

        except subp.CalledProcessError as e:
            print('ERROR in shell: ' + str(e))

        if code == 251:
            print('HORCM instance not started or working correctly...')
        else:
            if opt < 1:
                # this option dose not skip header line or the first row.
                df = pd.read_csv(out, engine='python', header=None, sep='[')
            else:
                # this option skips the header line or the first row.
                df = pd.read_csv(out, engine='python', header=None, skiprows=1, sep='\\s+')

        return df



    # Set Classes
    def set_command(self, cmd):
        self.command = cmd

    def set_copy(self, copy):
        self.copy = copy

    def set_horcmID(self, horcmid):
        self.horcmID = horcmid

    def set_horcm_group(self, horc_group):
        self.horcmGrp = horc_group

    def set_logpath(self, path):
        self.logpath = path

    def set_mail_address_to(self, mailaddr):
        self.mail_address_to = mailaddr

    def set_mail_subject(self, subj):
        self.mail_subject = subj

    def set_mail_message(self, msg):
        self.mail_message = msg

    def set_mail_smtp_server(self, smtpsrv):
        self.mail_smtp_server = smtpsrv

    def set_server(self, srv):
        self.server = srv

    def set_site(self, site):
        self.site = site

    def set_srcdest(self, srcdest):
        self.srcdest = srcdest

    # Get Classes
    def get_command(self):
        return self.command

    def get_copy(self):
        return self.copy

    def get_horcmID(self):
        return self.horcmID

    def get_horcm_group(self):
        return self.horcmGrp

    def get_logpath(self):
        return self.logpath

    def get_mail_address_to(self):
        return self.mail_address_to

    def get_mail_subject(self):
        return self.mail_subject

    def get_mail_message(self):
        return self.mail_message

    def get_mail_smtp_server(self):
        return self.mail_smtp_server

    def get_server(self):
        return self.server

    def get_site(self):
        return self.site

    def get_srcdest(self):
        return self.srcdest